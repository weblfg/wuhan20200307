# wuhan2020 黑客马拉松比赛—产品名称：认识病毒（KC-19）
### 此次wuhan2020 开源项目的主题是“疫情后社会的挑战”
* 发起人
    * 苏建勋&陈诺&刘付刚&齐泽文&Asimova&Heroza
    * 网站设计人员：刘付刚
    * 疫情小机器人对接微信开放平台对话功能人员：陈诺
    * Heroza 主要是外联人员
* 队员
    * 信息搜集人员：郝东霞，何钰婷，王仁宇，Pennie，折光润，Nick
* 产品信息
    * 一个主要面向海外的新冠病毒的资讯网站，为填补国外与国内对冠状病毒了解的信息断层。减少国外对新冠病毒的误解。包含对病毒的科普，康复者故事，以及疫情交流社区。同时配有AI小助手，医疗，靠谱的志愿者团队，物资信息，都可以直接找他核实。
    ![avatar](./src/assets/product.jpg)
    * 初赛证
    ![avatar](./src/assets/recode.png)


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
