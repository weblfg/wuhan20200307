const cn = {
  header: {
    headerTop: {
      text:
        "The novel coronavirus is a capsule with a segmented, single strand RNA virus. The particles are round or oval, and the diameter is about 60 to 140nm. They belong to the Betacoronavirus of the nested virus family.  the genome length of each group of the virus is about 30000 nucleotides. The  gene sequence shows that sars-cov-2 belongs to a virus with a long branch in the evolutionary tree of beta coronavirus lineage β (sarbecovirus),  is similar to the coronavirus found in the Chinese Rhinolophus, such as mers cov or SARS CoV.",
      text_Symptom: "症状",
      cough: "咳嗽",
      text_fever: "发烧",
      text_dyspnea: "呼吸困难",
      text_fali: "全身乏力",
      text_zhiliao: "预防与治疗",
      p1: "避免与患病的人近距离接触。",
      p2: "避免触碰自己的眼睛、鼻子和嘴巴",
      p3: "用家用清洁喷雾或消毒纸巾清洁消毒经常碰到",
      p4: "的物体表面。",
      p5: "保护自己 ",
      p6: "出门戴口罩",
      p7: "经常清洗双手",
      p8: "遮挡咳嗽",
      p9: "经常消毒高频接触物品"
    }
  }
  // ...zhLocale
};

export default cn;
