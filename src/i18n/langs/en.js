const en = {
  header: {
    headerTop: {
      text:
        "The novel coronavirus is a capsule with a segmented, single strand RNA virus. The particles are round or oval, and the diameter is about 60 to 140nm. They belong to the Betacoronavirus of the nested virus family.  the genome length of each group of the virus is about 30000 nucleotides. The  gene sequence shows that sars-cov-2 belongs to a virus with a long branch in the evolutionary tree of beta coronavirus lineage β (sarbecovirus),  is similar to the coronavirus found in the Chinese Rhinolophus, such as mers cov or SARS CoV.",
      text_Symptom: "Symptom",
      cough: "cough",
      text_fever: "Fever",
      text_dyspnea: "Shortness of breath",
      text_fali: "Fatigue",
      text_zhiliao: "Prevention  ",
      p1: "Avoid close contact with sick people.",
      p2: " Avoid touching your eyes, nose and mouth",
      p3: "Frequent cleaning of high frequency contact ",
      p4: "areas.",
      p5: "protect yourself",
      p6: "Wear masks when going out",
      p7: "Clean your hands often",
      p8: "Obscured cough",
      p9: "Frequently disinfect high frequency contact articles"
    }
  }
  // ...enLocale
};

export default en;
